angular.module('phonecatFilters', []).filter('suggestion', function() {
	return function(phone) {
	    return phone.age < 3 ? '\u2713' + phone.name : phone.name;
 	};
});