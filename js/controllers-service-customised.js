var phonecatApp = angular.module('phonecatApp', []);

phonecatApp.factory("loadService", ['$http', function($http) {
  return {
      "loadAll": function() { return $http.get('phones/phones.json');}
  }
}]);

phonecatApp.controller('PhoneListCtrl', ['$scope', 'loadService', function ($scope, loadService) {
  loadService.loadAll().success(function(data) {
    $scope.phones = data;
  });
  $scope.orderProp = 'age';
}]);


