var routeApp = angular.module("routeApp");

routeApp.controller("RouteListCtrl", function($scope) {
  $scope.articles = [
    {id:1, title:"Build-in service and customised service"},
    {id:2, title:"Route, baseline of one-page application"},
    {id:3, title:"Define your own directive"}
  ];
});

routeApp.controller("RouteDetailCtrl", function($scope, $routeParams) {
  $scope.id = $routeParams.id;
});

